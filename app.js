require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const requestPromise = require('request-promise')
//
const app = express()

// CONF
// error loging
// create a rolling file logger based on date/time that fires process events
let log = {}
const logger = require('simple-node-logger').createRollingFileLogger({
    logDirectory: process.env.LOG_DIR, // NOTE: folder must exist and be writable
    fileNamePattern:'<DATE>.log',
    dateFormat:'YYYY.MM.DD'
})
const VERIFICATION_URI = 'https://www.google.com/recaptcha/api/siteverify'


if (process.env.ENV === 'development') {
    log = {
        ...logger,
        error: msg => {
            console.error(msg)
            logger.error(msg)
        },
        debug: msg => {
            console.debug(msg)
            logger.debug(msg)
        },
        info: msg => {
            console.info(msg)
            logger.info(msg)
        }
    }
} else {
    logger.setLevel('error')
    log = logger
}

// verification request schema, must be sent with form's header
const verificationRequest = {
    secret: process.env.SECRET,
    response: '',
    remoteip: '',
}

const verify = function (response, remoteip) {
    const verification = requestPromise({
        method: 'POST',
        uri: VERIFICATION_URI,
        form: {
            ...verificationRequest,
            response,
            remoteip,
        },
    }).then(body => {
        const parsedBody = JSON.parse(body)
        if (parsedBody.success === false) throw parsedBody
        return parsedBody
    })
    return verification
}

const getReqIp = function (request) {
    return (
        request.headers['x-forwarded-for'] ||
        request.connection.remoteAddress ||
        request.socket.remoteAddress ||
        request.connection.socket.remoteAddress
    ).split(',')[0]
}

app.use(bodyParser.urlencoded({ extended: true }))

// enable cross origin requests
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

// GET route for ui xhr calls 
// app.get('/', async function(req, res) {

//     log.info('GET Request: started')

//     let verification
    
//     try {
//         const ip = getReqIp(req)
        
//         if (req.query.r === undefined) {
//             log.warn(`Client ${ip} sent a malformed request.`)
//             res.status(422).end()
//             return
//         }
        
//         verification = await verify(req.query.r, ip)
//     } catch (error) {
//         log.error(`GET Request: ${error}`)
//         res.status(500).end()
//         return
//     }
    
//     log.info('GET Request: verification ok')
//     res.json(verification)
// })

// POST route forwarding the actual data
app.post('/', async function(req, res) {
    
    log.info('POST Request: started')

    try {
        const ip = getReqIp(req)
    
        if (req.body.r === undefined) {
            res.status(422).end()
            return
        }
        
        await verify(req.body.r, ip)
        log.info('GET Request: verification ok')

        delete req.body.r

        requestPromise({
            method: 'POST',
            uri: process.env.TARGET_URL,
            form: req.body,
            followAllRedirects: true, // this line is google apps specyfic
        }).then(body => {
            log.debug(body)
        })

    } catch (error) {
        log.error(error)
        res.status(500).end()
        return
    }

    res.status(202).end()

})

app.listen(process.env.PORT)
